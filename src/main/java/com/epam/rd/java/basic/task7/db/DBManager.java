package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.epam.rd.java.basic.task7.db.entity.*;
import com.epam.rd.java.basic.task7.db.repository.IUser;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.management.relation.Role;
import javax.sql.DataSource;


public class DBManager implements IUser {

    private static final String CREATE_USER = "insert into users(login) values (?)";
    private static final String FIND_BY_FIELD_USER = "select * from users where login = ?";
    private static final String UPDATE_USER = "UPDATE users SET item=? WHERE id=?";
    private static final String DELETE_USER = "DELETE  FROM users WHERE id=?";
    private static final String FIND_ALL_USERS = "select * from users";


    private static final String CREATE_TEAMS = "insert into teams(name) values (?)";
    private static final String FIND_BY_FIELD_TEAMS = "select * from teams where name = ?";
    private static final String UPDATE_TEAM = "UPDATE teams SET item=? WHERE id=?";
    private static final String DELETE_TEAM = "DELETE  FROM teams WHERE id=?";
    private static final String FIND_ALL_TEAMS = "select * from teams";

    private static final String SET_User_Teams = "insert into users_teams(user_id, team_id)values(?,?)";
    private static final String GET_USER_TEAMS = "SELECT * FROM teams tm INNER JOIN users_teams utm on tm.id=utm.team_id WHERE utm.user_id = ?";


    private static DBManager instance;
    private static HikariConfig config;
    private static HikariDataSource ds;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        if (config == null) {

            config = new HikariConfig();
            //  config.setDriverClassName("com.mysql.cj.jdbc.Driver");
            // config.setJdbcUrl("jdbc:mysql://localhost:3306/teams_users_db");
            config.setJdbcUrl("jdbc:derby:memory:testdb;create=true");
            //  config.setUsername("root");
            // config.setPassword("root");

        }

        if (ds == null) {
            ds = new HikariDataSource(config);
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() {
        List<User> userList = new ArrayList<>();

        try (Connection con = ds.getConnection();
             PreparedStatement pst = con.prepareStatement(FIND_ALL_USERS);) {
            ResultSet result = pst.executeQuery();


            while (result.next()) {
                User user = new User();
                user.setId(result.getInt("id"));
                user.setLogin(result.getString("login"));

                userList.add(user);

            }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }


        return userList;

    }

    public boolean insertUser(User user) throws DBException {
        boolean response = false;

        if (user == null) {

            throw new DBException("user not found", new Exception());
        }


        try (Connection con = ds.getConnection();
             PreparedStatement pst = con.prepareStatement(CREATE_USER, Statement.RETURN_GENERATED_KEYS);) {
            //pst.setInt(1, user.getId());
            pst.setString(1, user.getLogin());


            int status = pst.executeUpdate();
            if (status != 1) {
                throw new DBException("user not created", new Exception(""));
            } else {
                response = true;
                ResultSet keys = pst.getGeneratedKeys();
                if(keys.next()){
                    user.setId(keys.getInt(1));
                }

            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }


        return response;
    }

    public boolean deleteUsers(User... users) throws DBException {

        try (Connection con = ds.getConnection();
             PreparedStatement pst = con.prepareStatement(DELETE_USER);) {
            for (User current : users) {
                pst.setInt(1, current.getId());
                pst.executeUpdate();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }


        return true;
    }

    public User getUser(String login) {

        User user = User.createUser(login);

        try (Connection con = ds.getConnection();
             PreparedStatement pst = con.prepareStatement(FIND_BY_FIELD_USER);) {

            pst.setString(1, login);

            ResultSet resultSet = pst.executeQuery();
            resultSet.next();


            user.setId(resultSet.getInt("id"));
            user.setLogin(resultSet.getString("login"));


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

    public Team getTeam(String name) {
        Team team = new Team();

        try (Connection con = ds.getConnection();
             PreparedStatement pst = con.prepareStatement(FIND_BY_FIELD_TEAMS);) {

            pst.setString(1, name);

            ResultSet resultSet = pst.executeQuery();
            resultSet.next();


            team.setId(resultSet.getInt("id"));
            team.setName(resultSet.getString("name"));


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return team;
    }

    public List<Team> findAllTeams() {
        List<Team> teamsList = new ArrayList<>();

        try (Connection con = ds.getConnection();
             PreparedStatement pst = con.prepareStatement(FIND_ALL_TEAMS);) {
            ResultSet result = pst.executeQuery();


            while (result.next()) {
                Team team = new Team();
                team.setId(result.getInt("id"));
                team.setName(result.getString("name"));

                teamsList.add(team);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return teamsList;
    }

    public boolean insertTeam(Team team) throws DBException {
        boolean resp = false;

        if (team == null) {

            throw new DBException("team not found", new Exception());
        }


        try (Connection con = ds.getConnection();
             PreparedStatement pst = con.prepareStatement(CREATE_TEAMS, Statement.RETURN_GENERATED_KEYS);) {
            //pst.setInt(1, team.getId());
            pst.setString(1, team.getName());


            int status = pst.executeUpdate();
            if (status != 1) {
                throw new DBException("team not created", new Exception(""));
            }
            else {
                resp = true;
                ResultSet keys = pst.getGeneratedKeys();
                if(keys.next()){
                    team.setId(keys.getInt(1));
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return resp;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        List<User> users = findAllUsers();
        List<Team> teamss  = getUserTeams(users.get(0));



        if (user == null || teams == null) {
            throw new DBException("user or teams are null", new Exception());
        }
        try (Connection con = ds.getConnection();
             PreparedStatement pst = con.prepareStatement(SET_User_Teams)) {
            con.setAutoCommit(false);

            for (Team current : teams) {
                if (current == null) {
                    con.rollback();
                    throw new DBException("", new NullPointerException());
                }

                pst.setInt(1, user.getId());
                pst.setInt(2, current.getId());
                pst.executeUpdate();

            }
            con.commit();


        } catch (Exception ex) {

            System.out.println(ex.getMessage());
            return false;
        }


        return true;
    }
//        if (user == null) throw new DBException("", new NullPointerException());
//        //String url = getUrl();
//        //try (Connection connection = DriverManager.getConnection(url);
//        ) {
//            connection.setAutoCommit(false);
//            try (PreparedStatement statement = connection.prepareStatement(SET_User_Teams)) {
//                for (Team team : teams) {
//                    if (team == null) {
//                        connection.rollback();
//                        throw new DBException("", new NullPointerException());
//                    }
//                    statement.setString(1, String.valueOf(user.getId()));
//                    statement.setString(2, String.valueOf(team.getId()));
//                    statement.executeUpdate();
//                }
//            } catch (SQLException throwables) {
//                connection.rollback();
//                throw new DBException("", throwables);
//            }
//            connection.commit();
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//            throw new DBException("", throwables);
//        }
//
//        return true;
//    }


    public List<Team> getUserTeams(User user) {
        List<Team> teamsList = new ArrayList<>();

        try (Connection con = ds.getConnection();
             PreparedStatement pst = con.prepareStatement(GET_USER_TEAMS);) {
            pst.setInt(1, user.getId());
            ResultSet result = pst.executeQuery();

            while (result.next()) {
                Team team = new Team();
                team.setId(result.getInt("id"));
                team.setName(result.getString("name"));

                teamsList.add(team);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return teamsList;
    }

    public boolean deleteTeam(Team team) throws DBException {
        boolean res = false;
        int id = team.getId();


        try (Connection con = ds.getConnection();
             PreparedStatement pst = con.prepareStatement(DELETE_TEAM);) {
            pst.setInt(1, id);

            int status = pst.executeUpdate();
            if (status == 1) {
                res = true;
            }
            if (status != 1) throw new DBException("", new Exception());

        } catch (Exception ex) {

        }


        return res;
    }

    public boolean updateTeam(Team team) throws DBException {
        Team team1 = new Team();
        boolean boolean_result = false;
        try (Connection con = ds.getConnection();
             PreparedStatement pst = con.prepareStatement(UPDATE_TEAM);) {
            pst.setInt(1, team1.getId());

            team1.setName(team.getName());


            int status = pst.executeUpdate();
            if (status != 1) throw new DBException("", new Exception());
            if (status == 1) {
                boolean_result = true;
            }


        } catch (Exception ex) {


        }


        return boolean_result;
    }

}
